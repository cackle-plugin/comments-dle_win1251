Инструкция по установке модуля Cackle комментариев для DLE v2.0:

Установка виджета комментариев

Шаг 1. Копирование файлов

1). Скопировать в fullstory.tpl (путь обычно templates/имя вашего шаблона) в конец шаблона след код:
<div class="dpad">
{include file="engine/modules/cackle/cackle_comments.php?newsid={news-id}"}
</div>
2). Скопировать папку cackle в engine/modules
3). Скопировать cackle_admin.php и cackle_moderate.php в engine/inc
4). Скопировать cackle.v201.min.js в engine/classes/js

Шаг 2. Ввод ключей в настройках модуля

 - Необходимо зарегистрироваться на Cackle по ссылке - http://bit.ly/1HCxkG9
 - Найти ключи из админ панели Cackle (Вкладка CMS платформа, клик по cms)
 - Открыть настройки модуля для dle по ссылке http://[корневой путь сайта]/admin.php?mod=cackle_admin  в браузере и ввести ключи из панели администрирования cackle.

FAQ по установке и использованию

Q: Не видно полей куда вводить ключи
A: Необходимо активировать на сервере pdo extention (обычно в php.ini разкомментировать строчки pdo, pdo_mysql).

Q: Откуда производить модерацию комментариев
A: Либо со своего сайта по адресу http://[корневой путь сайта]/admin.php?mod=cackle_moderate либо из админ панели Cackle на сайте admin.cackle.me

Установка виджета счетчика комментариев

1. заходим в \engine\modules\show.short.php
Ищем строчку - $tpl->set( '[com-link]', "<a href=\"" . $full_link . "#comment\">" );
Меняем на строчку - $tpl->set( '[com-link]', "<a cackle-channel=" . $row['id'] . " href=\"" . $full_link . "#mc-container\">" );
2. заходим в main.tpl своей темы и вставляем в конец перед тегом </body>
{include file="engine/modules/cackle/cackle_counter.php"}


Установка виджета последних комментариев

Необходимо вставить в нужное место шаблона следующий код:
{include file="engine/modules/cackle/cackle_recent.php"}