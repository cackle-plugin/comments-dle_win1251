<?php
include ('engine/api/api.class.php');
include ('engine/modules/cackle/cackle_sync.php');

require_once ENGINE_DIR.'/modules/cackle/cackle_activation.php';
require_once ENGINE_DIR.'/modules/cackle/request_handler.php';


$db_echo = $dle_api->install_admin_module("cackle_moderate","Cackle модерация", "Social comments for your site","cackle.png","all");
?>

<div id="mc-comment-admin"></div>
<script type="text/javascript">
    cackle_widget = window.cackle_widget || [];
    cackle_widget.push({widget: 'CommentAdmin', id: '<?php echo CackleAPI::cackle_get_param("cackle_apiId"); ?>'});
    (function () {
        var mc = document.createElement('script');
        mc.type = 'text/javascript';
        mc.async = true;
        mc.src = ('https:' == document.location.protocol ? 'https' : 'http') + '://cackle.me/widget.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(mc, s.nextSibling);
    })();
</script>



