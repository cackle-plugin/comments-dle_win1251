<?php
require_once(dirname(__FILE__) . '/cackle_api.php');

class CackleSync {
    function CackleSync() {
        $cackle_api = new CackleAPI();

    }

    function has_next ($size_comments, $size_pagination = 100) {
        return $size_comments == $size_pagination;
    }
    function push_next_comments($mode, $size_comments,$channel,$cackle_last_modified){
        //$time_start = microtime(true);
        $apix = new CackleAPI();
        $cackle_last_comment =  $apix->get_last_comment_by_channel($channel,0);
        $i = 1;
        while($this->has_next($size_comments)){
            $cackle_last_comment =  $apix->get_last_comment_by_channel($channel,0);
            $response = $apix->get_comments('last_comment',$cackle_last_comment,$channel,$i) ;
            $size_comments = $this->process_comments($response, $channel); // get comment from array and insert it to wp db
            $i++;
        }
        //$trace=debug_backtrace();
        //$function = $trace[0]["function"];
        //$mess='Function: ' . $function . ' execution_time' . (microtime(true) - $time_start)*1000 .PHP_EOL;
        //file_put_contents('execution_time.txt', $mess, FILE_APPEND);
    }
    function init($channel,$mode = "") {

        $apix = new CackleAPI();


        if($mode == ""){

            $object = CackleAPI::cackle_get_param('cackle_monitor');
            if($object === 0) $object = new stdClass();

            $object->post_id = $channel;
            $object->status = 'inprocess';
            $object->mode = 'by_channel';
            $object->time = time();
            CackleAPI::cackle_set_param('cackle_monitor',$object,true);
            $modified_triger = CackleAPI::cackle_get_param('cackle_modified_trigger');

            //Determine sync's criteria
            if(!isset($modified_triger->$channel) || (isset($modified_triger->$channel) && $modified_triger->$channel == 'c') ){
                $cackle_last_comment =  $apix->get_last_comment_by_channel($channel,0);
                $response = $apix->get_comments('last_comment', $cackle_last_comment,$channel);

            }

            if(isset($modified_triger->$channel) && $modified_triger->$channel == 'm'){
                $cackle_last_modified = $apix->get_last_modified_by_channel($channel,0);
                $response = $apix->get_comments('last_modified',$cackle_last_modified,$channel);
            }



            //get comments from Cackle Api for sync
            if ($response==NULL){
                return false;
            }

            $size_comments = $this->process_comments($response, $channel); // get comment from array and insert it to wp db, and return size
            if($this->has_next($size_comments)) {
                $object->status = 'next_page';
                $object->time = time();
                //$object->counter = $object->counter + 1;
                CackleAPI::cackle_set_param('cackle_monitor',$object,true);
            }
            else{
                $modified_triger->$channel = 'm';
                CackleAPI::cackle_set_param('cackle_modified_trigger',$modified_triger,true);

                //Update monitor status
                $object->status = 'finish';
                $object->time = time();
                //$object->counter = $object->counter + 1;
                CackleAPI::cackle_set_param('cackle_monitor',$object,true);
            }
        }


        if ($mode == "all_comments") {
            $cackle_last_comment =  $apix->get_last_comment_by_channel($channel,0);

            $object_s = CackleAPI::cackle_get_param('cackle_monitor_short');

            $object_s->post_id = $channel;
            $object_s->status = 'inprocess';
            $object_s->mode = 'all_comments';
            $object_s->time = time();
            CackleAPI::cackle_set_param('cackle_monitor_short',$object_s,true);

            //////////////////////////////////////////////////////////////////////

            $object = CackleAPI::cackle_get_param('cackle_monitor');

            $object->post_id = $channel;
            $object->status = 'inprocess';
            $object->mode = 'all_comments';
            $object->time = time();
            CackleAPI::cackle_set_param('cackle_monitor',$object,true);

            $response = $apix->get_comments('last_comment',$cackle_last_comment,$channel);

            //get comments from Cackle Api for sync
            if ($response==NULL){
                return false;
            }

            $size_comments = $this->process_comments($response, $channel); // get comment from array and insert it to wp db, and return size
            if ($this->has_next($size_comments)) {
                $this->push_next_comments($mode, $size_comments, $channel, $cackle_last_modified);
                $modified_triger = CackleAPI::cackle_get_param('cackle_modified_trigger');
                $modified_triger->$channel = 'm';
                CackleAPI::cackle_set_param('cackle_modified_trigger',$modified_triger,true);
            }
            else{
                //Initial sync completed
                $modified_triger = CackleAPI::cackle_get_param('cackle_modified_trigger');
                $modified_triger->$channel = 'm';
                CackleAPI::cackle_set_param('cackle_modified_trigger',$modified_triger,true);
            }
        }

        return "success";
    }

    /**
     * Decodes json to array
     * @return $obj
     */
    function cackle_json_decodes($response) {
        $obj = json_decode($response, true);
        return $obj;
    }

    /**
     * Get one comment from array $response and insert it to wp_comments throught insert_comm function
     * @return $obj
     */


    function process_comments($response,$channel) {
        //$time_start = microtime(true);

        $apix = new CackleAPI();
        $obj = $this->cackle_json_decodes($response,true);
        $obj = isset($obj['comments']) ? $obj['comments'] : array();
        $comments_size = count($obj);
        if ($comments_size != 0){
            $dbh = $apix->conn();
            //$dbh->beginTransaction();
            $parent_list = array();
            foreach ($obj as $comment) {
                if ($comment['id'] > $apix->get_last_comment_by_channel($channel,0)){
                    $comment_id = $comment['id'];
                    //$count = $wpdb->get_results($wpdb->prepare("SELECT count(comment_ID) as count from $wpdb->comments  WHERE comment_agent = %s", "Cackle:{$comment_id}"));
                   // if(isset($count[0]->count)&&$count[0]->count==0){

                        if ($this->startsWith($comment['chan']['channel'], 'http')) {
                            $postid = url_to_postid($comment['chan']['channel']);
                        } else {
                            $postid = $comment['chan']['channel'];
                        }

                        $commentdata = $this->insert_comm($comment, $this->comment_status_decoder($comment));


                        $apix->set_last_comment_by_channel($postid, $comment['id']);
                        if ($comment['modified'] > $apix->get_last_modified_by_channel($postid,0)) {
                            $apix->set_last_modified_by_channel($postid, $comment['modified']);

                        }
                    //}

                } else {
                    // if ($comment['modified'] > $apix->cackle_get_param('cackle_last_modified', 0)) {
                    $this->update_comment_status($comment['id'], $this->comment_status_decoder($comment), $comment['modified'], $comment['message'], $channel );
                    // }
                }
            }




            //$dbh->commit();

        }
        //$trace=debug_backtrace();
        //$function = $trace[0]["function"];
        //$mess='Function: ' . $function . ' execution_time' . (microtime(true) - $time_start)*1000 .PHP_EOL;
        //file_put_contents('execution_time.txt', $mess, FILE_APPEND);

        return $comments_size;
    }

    function startsWith($haystack, $needle) {
        $length = strlen($needle);
        return (substr($haystack, 0, $length) === $needle);
    }

    function comment_status_decoder($comment) {
        if (strtolower($comment['status']) == "approved") {
            $status = 1;
        } elseif (strtolower($comment['status'] == "pending") || strtolower($comment['status']) == "rejected") {
            $status = 0;
        } elseif (strtolower($comment['status']) == "spam") {
            $status = 0;
        } elseif (strtolower($comment['status']) == "deleted") {
            $status = 0;
        }
        return $status;
    }

    function update_comment_status($comment_id, $status, $modified, $comment_content,$channel) {
        //$time_start = microtime(true);
        $apix=new CackleAPI();
        $dbh = $apix->conn();

        $b = $dbh->prepare("UPDATE `".PREFIX.COMMENTS_TABLE."` SET approve=$status WHERE user_agent = 'Cackle:$comment_id'");
        $b->execute();

        $b = $dbh->prepare("UPDATE `".PREFIX.COMMENTS_TABLE."` SET comment='$comment_content' WHERE user_agent = 'Cackle:$comment_id'");
        $b->bindParam(":comment_content", $comment_content);
        $b->execute();

        if ($modified > $apix->get_last_modified_by_channel($channel,0)) {
            $apix->set_last_modified_by_channel($channel, $modified);
        }

        //$trace=debug_backtrace();
        //$function = $trace[0]["function"];
        //$mess='Function: ' . $function . ' execution_time' . (microtime(true) - $time_start)*1000 .PHP_EOL;
        //file_put_contents('execution_time.txt', $mess, FILE_APPEND);

    }

    function insert_comm($comment, $status) {
        //$time_start = microtime(true);

        $apix = new CackleAPI();

        if ($this->startsWith($comment['chan']['channel'], 'http')) {
            $postid = url_to_postid($comment['chan']['channel']);
        } else {
            $postid = $comment['chan']['channel'];
        }

        if (isset($comment['author']) && $comment['author'] != null) {
            $comment_author = isset($comment['author']['name']) ? $comment['author']['name'] : "";
            $comment_author_email = isset($comment['author']['email']) ? $comment['author']['email'] : "";
            $comment_author_url = isset($comment['author']['www']) ? $comment['author']['www'] : "" ;
            $author_avatar = isset($comment['author']['avatar']) ? $comment['author']['avatar']: "" ;
            if(isset($comment['author']['provider']) && $comment['author']['provider']=='sso'){
                if(isset($comment['author']['openId'])){
                    $openId = $comment['author']['openId'];
                    $user_id = (int)substr($openId, strpos($openId, "_") + 1);
                }

            }
            else{
                $user_id=0;
            }
        } else {
            $comment_author = isset($comment['anonym']['name']) ? $comment['anonym']['name'] : "";
            $author_avatar = "";
            if(!isset($comment['anonym']['email'])){
                $comment_author_email = "";
            }
            else{
                $comment_author_email = $comment['anonym']['email'];
            }
            $comment_author_url = isset($comment['anonym']['www']) ? $comment['anonym']['www'] : "";
        }

        $user_agent = 'Cackle:' . $comment['id'];
        $conn = $apix->conn();
        if (CACKLE_ENCODING=='cp1251'){

            $conn->exec('SET NAMES cp1251');
        }
        else{
            $conn->exec('SET NAMES utf8');
        }
        $cooment_fields = (object) unserialize(COMMENTS_FIELDS);
        if (isset($comment['author']['provider']) && $comment['author']['provider'] == 'sso') {
            if (isset($comment['author']['openId'])) {
                $openId = $comment['author']['openId'];
                $user_id = (int)substr($openId, strpos($openId, "_") + 1);
            }

        } else {
            $user_id = 0;
        }
        $sql = "insert into " . PREFIX .COMMENTS_TABLE."
        ($cooment_fields->post_id,$cooment_fields->autor,$cooment_fields->email,$cooment_fields->date,$cooment_fields->ip,$cooment_fields->comment,$cooment_fields->approve,$cooment_fields->user_agent,$cooment_fields->user_id)
        values
        (:channel, :author_name, :author_email, :date, :ip, :comment, :status, :user_agent, :user_id ) ";

        $q = $conn->prepare($sql);
        $q->execute(
            array(
                ':channel'=>(CACKLE_ENCODING=='cp1251') ? iconv("utf-8", "CP1251",$postid) : $postid,
                ':author_name'=>(CACKLE_ENCODING=='cp1251') ? iconv("utf-8", "CP1251",$comment_author) : $comment_author,
                ':author_email'=>(CACKLE_ENCODING=='cp1251') ? iconv("utf-8", "CP1251",$comment_author_email) : $comment_author_email ,

                ':date'=>strftime("%Y-%m-%d %H:%M:%S", $comment['created'] / 1000 + (CackleAPI::cackle_get_param('gmt_offset') * 3600)),
                ':ip'=>$comment['ip'],
                ':comment'=>(CACKLE_ENCODING=='cp1251') ? iconv("utf-8", "CP1251",$comment['message']) : $comment['message'],
                ':status'=>$status,
                ':user_agent'=>$user_agent,
                ':user_id'=>$user_id


            ));
        $q=null;
        //return $commentdata;

    }










    ////
    ////
    //
///////










}
?>