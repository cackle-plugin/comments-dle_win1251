
<?php
include ('engine/api/api.class.php');
//include ('engine/modules/cackle/cackle_sync.php');
$init_cackle = true;
require_once ENGINE_DIR.'/modules/cackle/cackle_comments.php';
require_once ENGINE_DIR.'/modules/cackle/cackle_activation.php';

require_once ENGINE_DIR.'/modules/cackle/request_handler.php';

define('HOME_URL', $config['http_home_url']);
$db_echo = $dle_api->install_admin_module("cackle_admin","Cackle �����������", "Social comments for your site","cackle.png","all");


?>
<!-- Angular Material Dependencies -->
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-type" content="text/html; charset=UTF-8" />
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/angular_material/0.11.0/angular-material.min.css">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.3.15/angular.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.3.15/angular-messages.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.3.15/angular-sanitize.js"></script>

    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.3.15/angular-animate.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.3.15/angular-aria.min.js"></script>

    <script src="//ajax.googleapis.com/ajax/libs/angular_material/0.11.0/angular-material.min.js"></script>
<!--    <script type="text/javascript" src="/engine/classes/js/cackle-admin/dev/cackle.min.js"></script>-->

<!--    <script type="text/javascript" src="/engine/classes/js/cackle-admin/dev/settings-module/index.js"></script>-->
<!--    <script type="text/javascript" src="/engine/classes/js/cackle-admin/dev/settings-module/controllers/index.js"></script>-->
<!--    <script type="text/javascript" src="/engine/classes/js/cackle-admin/dev/settings-module/controllers/settings.ctrl.js"></script>-->
<!--    <script type="text/javascript" src="/engine/classes/js/cackle-admin/dev/settings-module/services/index.js"></script>-->
<!--    <script type="text/javascript" src="/engine/classes/js/cackle-admin/dev/settings-module/services/cackle_api.js"></script>-->
<!--    <script type="text/javascript" src="/engine/classes/js/cackle-admin/dev/app.js"></script>-->

    <script type="text/javascript" src="/engine/classes/js/cackle.v201.min.js"></script>

</head>
<style>
    body {
        font-size: 1rem;
        font-font: arial;

    }

    span.md-subheader-content span {
        line-height: 16px !important;
        font-weight: 400;
    }

    div.md-subheader-inner {
        padding-top: 0 !important;;
    }

    h1 {
        font-size: 1.5rem;
    }

    #wpbody-content > div.error {
        display: none;
    }

    #wpwrap {
        background-color: #FFFFFF;

    }

    md-checkbox {
        margin: 0 !important;
    }

    md-list-item.disable-padding .md-no-style {
        padding: 0px !important;
        padding-left: 2px !important;
    }

    md-toolbar.cackle-errors, md-toolbar.cackle-errors .md-toolbar-tools {
        min-height: 36px !important;
        max-height: 36px !important;
    }

    md-toolbar.cackle-errors, md-toolbar.cackle-errors .md-toolbar-tools h1 span {
        font-size: 16px;
    }

    md-content.cackle-errors {
        font-size: 14px;
    }

    spinner svg {
        height: 32px;
        width: 32px;
    }

    .success {
        color: #008000;
    }

    .warn {
        color: #ff0000;
    }

    .mc-spin {
        display: inline-block;
        width: 16px !important;
        height: 16px !important;
        margin: 0 !important;
        padding: 0 !important;
        overflow: hidden !important;
        text-indent: 999em !important;
        vertical-align: middle !important;
        background: url(data:image/gif;base64,R0lGODlhEAAQAMQAAP///+7u7t3d3bu7u6qqqpmZmYiIiHd3d2ZmZlVVVURERDMzMyIiIhEREQARAAAAAP///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH/C05FVFNDQVBFMi4wAwEAAAAh+QQFBwAQACwAAAAAEAAQAAAFdyAkQgGJJOWoQgIjBM8jkKsoPEzgyMGsCjPDw7ADpkQBxRDmSCRetpRA6Rj4kFBkgLC4IlUGhbNQIwXOYYWCXDufzYPDMaoKGBoKb886OjAKdgZAAgQkfCwzAgsDBAUCgl8jAQkHEAVkAoA1AgczlyIDczUDA2UhACH5BAUHABAALAAAAAAPABAAAAVjICSO0IGIATkqIiMKDaGKC8Q49jPMYsE0hQdrlABCGgvT45FKiRKQhWA0mPKGPAgBcTjsspBCAoH4gl+FmXNEUEBVAYHToJAVZK/XWoQQDAgBZioHaX8igigFKYYQVlkCjiMhACH5BAUHABAALAAAAAAQAA8AAAVgICSOUGGQqIiIChMESyo6CdQGdRqUENESI8FAdFgAFwqDISYwPB4CVSMnEhSej+FogNhtHyfRQFmIol5owmEta/fcKITB6y4choMBmk7yGgSAEAJ8JAVDgQFmKUCCZnwhACH5BAUHABAALAAAAAAQABAAAAViICSOYkGe4hFAiSImAwotB+si6Co2QxvjAYHIgBAqDoWCK2Bq6A40iA4yYMggNZKwGFgVCAQZotFwwJIF4QnxaC9IsZNgLtAJDKbraJCGzPVSIgEDXVNXA0JdgH6ChoCKKCEAIfkEBQcAEAAsAAAAABAADgAABUkgJI7QcZComIjPw6bs2kINLB5uW9Bo0gyQx8LkKgVHiccKVdyRlqjFSAApOKOtR810StVeU9RAmLqOxi0qRG3LptikAVQEh4UAACH5BAUHABAALAAAAAAQABAAAAVxICSO0DCQKBQQonGIh5AGB2sYkMHIqYAIN0EDRxoQZIaC6bAoMRSiwMAwCIwCggRkwRMJWKSAomBVCc5lUiGRUBjO6FSBwWggwijBooDCdiFfIlBRAlYBZQ0PWRANaSkED1oQYHgjDA8nM3kPfCmejiEAIfkEBQcAEAAsAAAAABAAEAAABWAgJI6QIJCoOIhFwabsSbiFAotGMEMKgZoB3cBUQIgURpFgmEI0EqjACYXwiYJBGAGBgGIDWsVicbiNEgSsGbKCIMCwA4IBCRgXt8bDACkvYQF6U1OADg8mDlaACQtwJCEAIfkEBQcAEAAsAAABABAADwAABV4gJEKCOAwiMa4Q2qIDwq4wiriBmItCCREHUsIwCgh2q8MiyEKODK7ZbHCoqqSjWGKI1d2kRp+RAWGyHg+DQUEmKliGx4HBKECIMwG61AgssAQPKA19EAxRKz4QCVIhACH5BAUHABAALAAAAAAQABAAAAVjICSOUBCQqHhCgiAOKyqcLVvEZOC2geGiK5NpQBAZCilgAYFMogo/J0lgqEpHgoO2+GIMUL6p4vFojhQNg8rxWLgYBQJCASkwEKLC17hYFJtRIwwBfRAJDk4ObwsidEkrWkkhACH5BAUHABAALAAAAQAQAA8AAAVcICSOUGAGAqmKpjis6vmuqSrUxQyPhDEEtpUOgmgYETCCcrB4OBWwQsGHEhQatVFhB/mNAojFVsQgBhgKpSHRTRxEhGwhoRg0CCXYAkKHHPZCZRAKUERZMAYGMCEAIfkEBQcAEAAsAAABABAADwAABV0gJI4kFJToGAilwKLCST6PUcrB8A70844CXenwILRkIoYyBRk4BQlHo3FIOQmvAEGBMpYSop/IgPBCFpCqIuEsIESHgkgoJxwQAjSzwb1DClwwgQhgAVVMIgVyKCEAIfkECQcAEAAsAAAAABAAEAAABWQgJI5kSQ6NYK7Dw6xr8hCw+ELC85hCIAq3Am0U6JUKjkHJNzIsFAqDqShQHRhY6bKqgvgGCZOSFDhAUiWCYQwJSxGHKqGAE/5EqIHBjOgyRQELCBB7EAQHfySDhGYQdDWGQyUhADs=) 0 center no-repeat !important;
    }

</style>

<!--    Asset::getInstance()->addJs("https://ajax.googleapis.com/ajax/libs/angularjs/1.3.15/angular.min.js")-->


<?php


function getUtfMessage($mes){
    if (strtoupper(SITE_CHARSET) != "UTF-8") {
        $str = preg_replace_callback('/\\\u([a-f0-9]{4})/i', create_function('$m', 'return chr(hexdec($m[1])-1072+224);'), $mes);
        return iconv('cp1251', 'utf-8', $str);
    } else {
        return $mes;
    }
}




function GetMessageF($mes){
    return getUtfMessage($mes);
}

function filter_cp1251($string1){
    $str = preg_replace_callback('/\\\u([a-f0-9]{4})/i', create_function('$m', 'return chr(hexdec($m[1])-1072+224);'), $string1);
    return iconv('cp1251', 'utf-8', $str);
}


$settings[] = array(
    'siteId' => CackleAPI::cackle_get_param('cackle_apiId'),
    'siteApiKey' => CackleAPI::cackle_get_param('cackle_siteApiKey'),
    'accountApiKey' => CackleAPI::cackle_get_param('cackle_accountApiKey'),
    'sso' => CackleAPI::cackle_get_param('cackle_sso') ? true : false,
    'sync' => CackleAPI::cackle_get_param('cackle_sync') ? true : false,
    'manual_sync' => CackleAPI::cackle_get_param('cackle_manual_sync'),
    'manual_export' => CackleAPI::cackle_get_param('cackle_manual_export'),
    'allow_url_fopen_error' => (ini_get('allow_url_fopen') != '1') ? true : false,
    'curl_exist_error' => !function_exists('curl_version') ? true : false,
    'curl_openbase_error' => (ini_get('open_basedir') != '') ? true : false,
    'curl_safemode_error' => (ini_get('safe_mode') == true) ? true : false
);
$settings = json_encode($settings);

function GetMessage($mes){
    return $mes;
}
?>

<script type="application/javascript">
    cackle_admin = {};
    cackle_admin.settings = JSON.parse('<?php echo($settings)?>')[0];
    cackle_admin.url = '';
    cackle_admin.status= {
        '�������� ����� ��� ���������': '<?php print_r(CackleAPI::cackle_get_param('cackle_whitelabel')) ?>',
        '����������� �������': '<?php print_r(CackleAPI::cackle_get_param('cackle_lang')) ?>',
        '�������� ����� ������ �����������': '<?php print_r(CackleAPI::cackle_get_param('cackle_paidsso')) ?>',
        '������ ��������� ������': '<?php print_r(CackleAPI::cackle_get_param('cackle_correctKey')) ?>'
    };
    cackle_locale={
        'Cackle plugin installation': '<?php print_r(GetMessage('��������� ������ Cackle')) ?>',
        'Check status': '<?php print_r(GetMessage('�������� ������� ������')) ?>',
        'Export comments' : '<?php print_r(GetMessage('������� ������������')) ?>',
        'Sync comments' : '<?php print_r(GetMessage('������������� ������������')) ?>',
        'Cackle plugin status and account availiable options' : '<?php print_r(GetMessage('������ ��������� ������ � �������� ����� �������� Cackle')) ?>',
        'Warnings and errors' : '<?php print_r(GetMessage('������, ��������������, �����������')) ?>',
        'Enable SSO' : '<?php print_r(GetMessage('������ �����������(���)')) ?>',
        'Enable sync(SEO)' : '<?php print_r(GetMessage('�������������(���)')) ?>',
        'Activate' : '<?php print_r(GetMessage('������������')) ?>',
        'Plugin activated' : '<?php print_r(GetMessage('������ ��������� ������')) ?>',
        'Single sign on' : '<?php print_r(GetMessage('������ �����������')) ?>',
        'This will export your existing WordPress comments to Cackle' : '<?php print_r(GetMessage('������� ������������ � �� DLE ������������ � ������ Cackle')) ?>',
        'This will download your Cackle comments and store them locally in WordPress' : '<?php print_r(GetMessage('������������� ������������ � ������� Cackle � ��������� �� DLE')) ?>',
        'Start' : '<?php print_r(GetMessage('������')) ?>',
        'Continue' : '<?php print_r(GetMessage('����������')) ?>',
        'Stop' : '<?php print_r(GetMessage('����������')) ?>',
        'Cackle widget language' : '<?php print_r(GetMessage('����������� �������')) ?>',
        'Export process' : '<?php print_r(GetMessage('������� ��������')) ?>',
        'Sync process' : '<?php print_r(GetMessage('������� �������������')) ?>',
        'export_Processed comments for post_id = ' : '<?php print_r(GetMessage('������� ������������ ��� ����� � id = ')) ?>',
        'export_Processed comments was stopped for post_id = ' : '<?php print_r(GetMessage('������� ������������ ��� ���������� ��� ����� � id = ')) ?>',
        'export_All comments were transfer successfully to Cackle!' : '<?php print_r(GetMessage('��� ����������� ���� ������� �������������� � Cackle!')) ?>',
        'sync_Processed comments for post_id = ' : '<?php print_r(GetMessage('������������� ������������ ��� ����� � id = ')) ?>',
        'sync_Processed comments was stopped for post_id = ' : '<?php print_r(GetMessage('������������� ������������ ���� �������� ��� ����� � id = ')) ?>',
        'sync_All comments were transfer successfully to Cackle!' : '<?php print_r(GetMessage('��� ����������� ���� ������� �������������� � �� DLE!')) ?>',
        'allow_url_fopen_error' : '<?php print_r(GetMessage('��� ���������� ������ ������������� �������� allow_url_fopen  ������ ���� ����������. <br> 1. �������� ���� php.ini (�� ������� /etc/php5/cli/php.ini) <br>2. ������� �������� allow_url_fopen � ���������� �������� On, � ����� ������ ���������� <b>allow_url_fopen = On</b> � ������������� ������ ��� ���������� ��������� <br>3. ��������� � Cackle ��������� � �������� ������ ������������.')) ?>',
        'Warning' : '<?php print_r(GetMessage('��������������')) ?>',
        'Success' : '<?php print_r(GetMessage('�������� ���������!')) ?>',
        'Plugin was successfully activated!' : '<?php print_r(GetMessage('������ ��� ������� �����������!')) ?>',
        'The entered keys are wrong. Please check it again. Plugin was not activated' : '<?php print_r(GetMessage('������� �������� �����. ��������� ������������ ����� ������. ������ �� �����������!')) ?>',
        'Plugin was successfully activated' : '<?php print_r(GetMessage('������ ��� ������� �����������')) ?>',
        'Plugin was not activated, check keys' : '<?php print_r(GetMessage('������ �� ��� �����������, ��������� ����� � ����������� �����')) ?>',
        'Paid Single Sign On option' : '<?php print_r(GetMessage('�������� ����� ������ �����������')) ?>',
        'Paid white label option' : '<?php print_r(GetMessage('�������� ����� ��� ���������')) ?>',
        'Posts prepared for import' : '<?php print_r(GetMessage('���������� ������ ������ ��� ������������� ')) ?>',
        'with specified error: ' : '<?php print_r(GetMessage(' �� ��������� �������: ')) ?>',
        'Error 500. Unable to connect server. Check server or internet' : '<?php print_r(GetMessage('������ 500. ���������� ����������� � ��������. ��������� ������ ������� ��� �������� ����������')) ?>',
        'Unable to connect with Cackle' : '<?php print_r(GetMessage('���������� ��������� � �������� Cackle')) ?>',
        'Last successfull exported comments was for post_id = ' : '<?php print_r(GetMessage('��������� ������ ���������������� ����������� ��� ����� � id = ')) ?>',
        'Last successfull synced comments was for post_id = ' : '<?php print_r(GetMessage('��������� ������ ���������������� ����������� ��� ����� � id = ')) ?>',

        'curl_safemode_error' : '<?php print_r(GetMessage('��� ���������� ������ ������������� �������� safe mode ������ ���� ��������.<br> 1. �������� ���� php.ini (�� ������� /etc/php5/cli/php.ini) <br>2. ������� ��������  safe_mode � ���������� �������� off, � ����� ������ ���������� <b> safe_mode = off</b> <br>3. ��������� � Cackle ��������� � �������� ������ ������������.')) ?>',
        'curl_openbase_error' : '<?php print_r(GetMessage('��� ���������� ������ ������������� �������� open_basedir ������ ���� ������. <br> 1. �������� ���� php.ini (�� ������� /etc/php5/cli/php.ini) <br>2. ������� �������� open_basedir � ��������������� ���, � ����� ������ ���������� <b>;openbase_dir</b> <br>3. ��������� � Cackle ��������� � �������� ������ ������������.')) ?>',
        'curl_exist_error' : '<?php print_r(GetMessage('��� �������� ������ ������������� ��� ���������� ������������ ���������� curl ��� php � ���������� ������ �������. <br> 1. �������� ���� php.ini (�� ������� /etc/php5/cli/php.ini) <br>2. ������� ������ �  <b>;extension=php_curl.dll </b> � ���������������� ��, � ����� ������ ���������� <b>extension=php_curl.dll</b> <br>3. ��������� � Cackle ��������� � �������� ������ ������������.')) ?>',
        '(comments)' : '<?php print_r(GetMessage('������������')) ?>'


    };




</script>

<div ng-app="cackle-admin.Angular">
    <div ng-controller="settings.ctrl">
        <div ng-include src="'main.html'"></div>
    </div>

    <script type="text/ng-template" id="main.html">

        <div layout-margin="10" layout="row" layout-sm="column" layout-md="column" layout-wrap>
            <div class="md-whiteframe-z1" flex-sm flex-md flex>

                <md-toolbar class="md-primary md-default-theme">
                    <div class="md-toolbar-tools">
                        <h1>
                            <span>{{ locale['Cackle plugin installation']}}</span>
                        </h1>
                    </div>
                </md-toolbar>


                <md-content layout-padding md-default-theme>
                    <form name="userForm">

                        <div layout-sm="column">
                            <md-input-container>
                                <label for="input-1">Widget ID</label>
                                <input type="text" id="input-1" ng-model="initData.siteId">
                            </md-input-container>

                        </div>
                        <div layout-sm="column">
                            <md-input-container>
                                <label for="inputId">accountApiKey</label>
                                <input name="accountApiKey" type="text" ng-model="initData.accountApiKey" required
                                       md-maxlength="64" minlength="4">

                                <div ng-messages="userForm.accountApiKey.$error"
                                     ng-show="userForm.accountApiKey.$dirty">
                                    <div ng-message="required">This is required!</div>
                                    <div ng-message="md-maxlength">That's too long!</div>
                                    <div ng-message="minlength">That's too short!</div>
                                </div>
                            </md-input-container>

                        </div>
                        <div layout-sm="column">
                            <md-input-container>
                                <label for="inputId">siteApiKey</label>
                                <input name="siteApiKey" type="text" ng-model="initData.siteApiKey" required
                                       md-maxlength="64" minlength="4">

                                <div ng-messages="userForm.siteApiKey.$error" ng-show="userForm.siteApiKey.$dirty">
                                    <div ng-message="required">This is required!</div>
                                    <div ng-message="md-maxlength">That's too long!</div>
                                    <div ng-message="minlength">That's too short!</div>
                                </div>
                            </md-input-container>

                            <md-list>
                                <md-list-item class="">

                                    <p>{{locale['Enable SSO']}}</p>
                                    <md-checkbox ng-model="initData.sso" class="md-primary"></md-checkbox>

                                </md-list-item>
                                <md-list-item class="">
                                    <p>{{locale['Enable sync(SEO)']}}</p>
                                    <md-checkbox ng-model="initData.sync" class="md-primary"></md-checkbox>
                                </md-list-item>


                            </md-list>
                            <md-button ng-click="activate()" class="md-raised md-primary">{{locale['Activate']}}
                            </md-button>
                            <md-subheader ng-if="object_keys(messages).length>0" class="md-no-sticky">{{locale['Warnings
                                and errors']}}
                            </md-subheader>
                            <div ng-repeat="message in messages">
                                <div error-message error="message.text" header="message.header"
                                     errorclass="message.class"></div>
                            </div>
                        </div>

                    </form>
                </md-content>

            </div>
            <div class="md-whiteframe-z1" flex-sm flex>

                <md-toolbar class="md-primary md-default-theme">
                    <div class="md-toolbar-tools">
                        <h1>
                            <span ng-bind="locale['Check status']"></span>
                        </h1>
                    </div>
                </md-toolbar>


                <md-content layout-padding md-default-theme>
                    <md-list>
                        <md-list-item>
                            <h2 ng-if="status[locale['Plugin activated']]||status['correctKey']==true">
                                {{locale['Plugin was successfully activated']}}</h2>

                            <h2 ng-if="status[locale['Plugin activated']]==false || status['correctKey']==false">
                                {{locale['Plugin was not activated, check keys']}}</h2>
                        </md-list-item>
                    </md-list>
                    <md-list>
                        <md-subheader class="md-no-sticky">
                            {{locale['Cackle plugin status and account availiable options']}}
                        </md-subheader>

                        <md-list-item
                            ng-repeat="(k,v) in (filtered = (status|objToArray:locale['Cackle widget language']))">

                            <p ng-hide="k=='correctKey'||k=='whitelabel'||k=='sso'||k=='lang'"> {{ k }} </p>


                            <span ng-show="k==locale['Cackle widget language']">{{v}}</span>

                            <p ng-show="k=='correctKey'">{{locale['Plugin activated']}}</p>
                            <p ng-show="k=='whitelabel'">{{locale['Paid white label option']}}</p>
                            <p ng-show="k=='sso'">{{locale['Paid Single Sign On option']}}</p>
                            <p ng-show="k=='lang'">{{locale['Cackle widget language']}}</p>
                            <span ng-show="k=='lang'">{{v}}</span>
                            <md-checkbox ng-disabled="true" ng-hide="k==locale['Cackle widget language']||k=='lang'"
                                         class="md-primary" ng-model="v"></md-checkbox>
                        </md-list-item>
                        <md-divider></md-divider>


                    </md-list>

                </md-content>

            </div>

        </div>
        <div layout-margin="10" layout="row" layout-wrap layout-sm="column"  layout-md="column">
            <div class="md-whiteframe-z1" flex-sm flex flex-md>
                <md-toolbar class="md-primary md-default-theme">
                    <div class="md-toolbar-tools">
                        <h1>
                            <span>{{locale['Export comments']}}</span>
                        </h1>
                    </div>
                </md-toolbar>
                <md-content layout-padding>
                    <p>{{locale['This will export your existing WordPress comments to Cackle']}}</p>
                    <md-button  ng-disabled="transfer['export'].status" ng-click="transferStart('export','start')" class="md-raised md-primary">{{locale['Start']}}
                    </md-button>
                    <md-button  ng-disabled="transfer['export'].status" ng-click="transferStart('export','continue')" class="md-raised md-primary">{{locale['Continue']}}
                    </md-button>
                    <md-button ng-click="transferStop('export')" class="md-raised md-primary">{{locale['Stop']}}
                    </md-button>

                    <md-list>
                        <md-subheader ng-show="transfer['export']" class="md-no-sticky">{{locale['Export process']}}</md-subheader>
                        <md-list-item ng-show="transfer['export'].spinner"><spinner></spinner></md-list-item>
                        <md-list-item ng-repeat="mess in ((transfer['export'].messages|limitTo:-5)) track by $index">
                            <span ng-bind-html="mess"></span>
                        </md-list-item>
                    </md-list>
                </md-content>

            </div>
            <div class="md-whiteframe-z1" flex-sm flex>
                <md-toolbar class="md-primary md-default-theme">
                    <div class="md-toolbar-tools">
                        <h1>
                            <span>{{locale['Sync comments']}}</span>
                        </h1>
                    </div>
                </md-toolbar>
                <md-content layout-padding>
                    <p>{{locale['This will download your Cackle comments and store them locally in WordPress']}}</p>
                    <md-button ng-disabled="transfer['sync'].status" ng-click="initCommentsPrepare()" class="md-raised md-primary">{{locale['Start']}}
                    </md-button>
                    <md-button  ng-disabled="transfer['sync'].status" ng-click="transferStart('sync','continue')" class="md-raised md-primary">{{locale['Continue']}}
                    </md-button>
                    <md-button ng-click="transferStop('sync')" class="md-raised md-primary">{{locale['Stop']}}
                    </md-button>
                    <p ng-show="commentLoading">{{locale['Posts prepared for import']}} <b>{{comments_prepared_counter}}</b></p>
                    <md-list>
                        <md-subheader ng-show="transfer['sync']" class="md-no-sticky">{{locale['Sync process']}}</md-subheader>
                        <md-list-item ng-show="transfer['sync'].spinner"><spinner></spinner></md-list-item>
                        <md-list-item ng-repeat="mess in ((transfer['sync'].messages|limitTo:-5)) track by $index">
                            <span ng-bind-html="mess"></span>
                        </md-list-item>
                    </md-list>
                </md-content>
            </div>
        </div>


    </script>

    <script type="text/ng-template" id="scopeTemplate">
        <div layout="row">
            <div flex-sm flex class="md-whiteframe-z5" layout-margin>

                <md-toolbar class="cackle-errors {{errorClass}}">
                    <div class="md-toolbar-tools">
                        <h1>
                            <span>{{header}}</span>
                        </h1>
                    </div>
                </md-toolbar>
                <md-content class="cackle-errors" layout-padding ng-bind-html="error">

                </md-content>


            </div>
        </div>
    </script>
</div>




